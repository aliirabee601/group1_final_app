import 'package:flutter/material.dart';

Widget textField(
    {String hint, TextInputType type, Function validator, Function onSave}) {
  return Padding(
    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
    child: TextFormField(
      validator: validator,
      onSaved: onSave,
      keyboardType: type,
      decoration: InputDecoration(
          hintText: hint,
          contentPadding: EdgeInsets.symmetric(horizontal: 6),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10))),
    ),
  );
}
