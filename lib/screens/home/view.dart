import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import '../categories/view.dart';
import '../competition/view.dart';
import '../jobs/view.dart';
import '../more/view.dart';
import '../news/view.dart';

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {

  List<Map<String, dynamic>> _catsData = [
    {
      'title': 'تطوع معنا',
      'image': 'assets/images/tatawaaImage.png',
      'catImage': 'assets/images/tatawaa.png'
    },
    {
      'title': 'الاخبار',
      'image': 'assets/images/newsImage.png',
      'catImage': 'assets/images/news.png'
    },
    {
      'title': 'المسابقة',
      'image': 'assets/images/newsImage.png',
      'catImage': 'assets/images/tatawaa.png'
    },
    {
      'title': 'المزيد',
      'image': 'assets/images/moreImage.png',
      'catImage': 'assets/images/tatawaa.png'
    }
  ];
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
          backgroundColor: Theme.of(context).backgroundColor,
          appBar: AppBar(
            backgroundColor: Theme.of(context).backgroundColor,
            elevation: 0,
            centerTitle: false,
            automaticallyImplyLeading: false,
            actions: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 20,
                ),
                child: Image.asset(
                  'assets/images/logo.png',
                  width: 30,
                  height: 20,
                ),
              )
            ],
            title: Text(
              'الرئيسية',
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 19),
            ),
          ),
          body: Column(
            children: <Widget>[
              InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => CategoriesView()));
                },
                child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: 160,
                    margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage('assets/images/cat1.png'),
                          fit: BoxFit.cover),
                      borderRadius: BorderRadius.circular(12),
                      color: Theme.of(context).primaryColor,
                    ),
                    child: Stack(
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width,
                          height: 160,
                          // padding: EdgeInsets.symmetric(
                          //     horizontal: 20, vertical: 20),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12),
                            color:
                                Theme.of(context).primaryColor.withOpacity(0.9),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Image.asset(
                                'assets/images/cats.png',
                                width: 100,
                                height: 100,
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 30, vertical: 10),
                                child: Text(
                                  'اقسام اللجنة',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16),
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    )),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 5),
                  child: StaggeredGridView.countBuilder(
                    crossAxisCount: 4,
                    itemCount: 4,
                    itemBuilder: (BuildContext context, int index) => _item(
                        index: index,
                        image: _catsData[index]['image'],
                        title: _catsData[index]['title'],
                        catImage: _catsData[index]['catImage']),
                    staggeredTileBuilder: (int index) =>
                        StaggeredTile.count(2, index.isEven ? 3 : 2),
                    mainAxisSpacing: 4.0,
                    crossAxisSpacing: 4.0,
                  ),
                ),
              )
            ],
          )),
    );
  }

  Widget _item({int index, String title, String catImage, String image}) {
    return InkWell(
        onTap: () {
          if (index == 0) {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => JobsView()));
          } else if (index == 1) {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => NewsView()));
          } else if (index == 2) {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => CompetitionView()));
          } else {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => MoreView()));
          }
        },
        child: Stack(
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
              margin: EdgeInsets.all(10),
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage(image), fit: BoxFit.cover),
                  color: Theme.of(context).accentColor,
                  borderRadius: BorderRadius.circular(12)),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.all(10),
              decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor.withOpacity(0.9),
                  borderRadius: BorderRadius.circular(12)),
            ),
            Padding(
              padding: EdgeInsets.only(
                top: 5,
                bottom: 19,
                right: 0,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Image.asset(
                    catImage,
                    width: 120,
                    height: 120,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 35, bottom: 10),
                    child: Text(
                      title,
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 16),
                    ),
                  )
                ],
              ),
            ),
          ],
        ));
  }
}
