import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:lagnet_eltanmmya/helpers/text_field.dart';

class CompetitionView extends StatefulWidget {
  @override
  _CompetitionViewState createState() => _CompetitionViewState();
}

class _CompetitionViewState extends State<CompetitionView> {
  String _name, _phone, _email;
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  void _submitForm() {
    if (!_formKey.currentState.validate()) {
      return;
    }
    _formKey.currentState.save();
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          centerTitle: false,
          title: Text(
            'المسابقه',
            style: TextStyle(color: Colors.black),
          ),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: Colors.black,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          backgroundColor: Colors.white,
          elevation: 0,
        ),
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 20, top: 10, bottom: 20),
              child: Row(
                children: [
                  Text(
                    'للاشتراك ادخل البيانات الاتيه',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  )
                ],
              ),
            ),
            Form(
                key: _formKey,
                child: Column(
                  children: [
                    textField(
                        hint: 'الاسم الرباعي',
                        type: TextInputType.text,
                        validator: (value) {
                          if (value.toString().isEmpty) {
                            return 'Name is required';
                          } else {
                            return null;
                          }
                        },
                        onSave: (value) {
                          setState(() {
                            _name = value;
                          });
                        }),
                    textField(
                        hint: 'رقم الجوال',
                        type: TextInputType.number,
                        validator: (value) {
                          if (value.toString().isEmpty) {
                            return 'Phone is required';
                          } else {
                            return null;
                          }
                        },
                        onSave: (value) {
                          setState(() {
                            _phone = value;
                          });
                        }),
                    textField(
                        hint: 'البريد الالكتروني',
                        type: TextInputType.emailAddress,
                        validator: (value) {
                          if (value.toString().isEmpty) {
                            return 'Email is required';
                          } else {
                            return null;
                          }
                        },
                        onSave: (value) {
                          setState(() {
                            _email = value;
                          });
                        }),
                  ],
                )),
            Expanded(child: Container()),
            InkWell(
              onTap: () {
                _submitForm();
              },
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 50,
                margin: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Theme.of(context).primaryColor),
                child: Center(
                  child: Text(
                    'اشترك الان',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
