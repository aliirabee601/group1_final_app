import 'package:flutter/material.dart';

import '../job_details/view.dart';
import 'controller.dart';
import 'model.dart';

class JobsView extends StatefulWidget {
  @override
  _JobsViewState createState() => _JobsViewState();
}

class _JobsViewState extends State<JobsView> {
  JobsController _jobsController = JobsController();
  JobsModel _jobsModel = JobsModel();
  bool _loading = true;

  @override
  void initState() {
    _getJobs();
    super.initState();
  }

  _getJobs() async {
    _jobsModel = await _jobsController.getJobs();
    setState(() {
      _loading = false;
    });
  }

  Widget _item({String image, String title, int id}) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => JobDetailsView(
                      jobId: id,
                    )));
      },
      child: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: NetworkImage(image), fit: BoxFit.cover),
                borderRadius: BorderRadius.circular(20),
                color: Colors.red),
          ),
          Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Colors.black.withOpacity(0.4)),
          ),
          Align(
              alignment: Alignment.center,
              child: Text(
                title,
                style: TextStyle(color: Colors.white),
              ))
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          centerTitle: false,
          title: Text(
            'فرص التطوع',
            style: TextStyle(color: Colors.black),
          ),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: Colors.black,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          backgroundColor: Colors.white,
          elevation: 0,
        ),
        body: _loading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Padding(
                padding: const EdgeInsets.all(5.0),
                child: GridView.builder(
                    itemCount: _jobsModel.data.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        mainAxisSpacing: 5,
                        crossAxisSpacing: 5),
                    itemBuilder: (ctx, index) {
                      return _item(
                          id: _jobsModel.data[index].id,
                          image: _jobsModel.data[index].image,
                          title: _jobsModel.data[index].title);
                    }),
              ),
      ),
    );
  }
}
