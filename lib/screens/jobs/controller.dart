import 'dart:convert';

import 'package:dio/dio.dart';

import 'model.dart';

class JobsController {
  Dio _dio = Dio();
  JobsModel _jobsModel = JobsModel();
  Future<JobsModel> getJobs() async {
    var response =
        await _dio.get('https://association.rowadtqnee.sa/api/opportunities');
    var data = json.decode(response.toString());
    _jobsModel = JobsModel.fromJson(data);
    return _jobsModel;
  }
}
