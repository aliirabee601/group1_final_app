import 'package:flutter/material.dart';
import 'package:lagnet_eltanmmya/screens/splash/view.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SplashView(),
      theme: ThemeData(
          fontFamily: 'Tajawal',
          backgroundColor: Color.fromRGBO(242, 242, 242, 1),
          primaryColor: Color.fromRGBO(48, 70, 55, 1),
          accentColor: Color.fromRGBO(211, 173, 106, 0.3)),
    );
  }
}
